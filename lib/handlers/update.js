'use strict';

const Boom = require('boom');

exports.run = async function (request) {

    const server = request.server;
    const knex = server.app.knex;

    if (request.payload.password === request.payload.repassword) {
        return knex
            .then((knexInstance) => {

                return knexInstance('crud')
                    .where('email', '=', request.payload.email)
                    .update({

                        password: request.payload.password
                    })
                    .then(() => {

                        return Promise.resolve('updated successfully');
                    })
            })
            .catch((error) => {

                return Promise.resolve(Boom.badRequest(error.detail));
            });
    }
    else {
            return Promise.resolve(Boom.badRequest('Passwords does not match'));
    }
};
