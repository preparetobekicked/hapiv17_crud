'use strict';

const Boom = require('boom');

exports.run = async function (request) {

    const server = request.server;
    const knex = server.app.knex;

    const payload = {
        name: request.payload.name,
        password: request.payload.password,
        email: request.payload.email
    };

    if (request.payload.password === request.payload.repassword) {
        return knex
            .then((knexInstance) => {

                return knexInstance.insert(payload).into('crud')
                    .then(() => {

                        return Promise.resolve('added successfully');
                    })
            })
            .catch((e) => {

                return Promise.resolve(Boom.badRequest(e.detail));
            });
    }
    else {
        return Promise.resolve(Boom.badRequest('Passwords does not match'));
    }
};
