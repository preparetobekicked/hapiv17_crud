'use strict';

const Boom = require('boom');

exports.run = async function (request) {

    const server = request.server;
    const knex = server.app.knex;

    return knex
        .then((knexInstance) => {

            return knexInstance('crud')
                .where('email', request.payload.email)
                .del()
                .then(() => {

                    return Promise.resolve('successfully deleted');
                })
        })
        .catch((e) => {

            return Promise.resolve(Boom.badRequest(e.detail));
        });
};
