'use strict';

const Boom = require('boom');

exports.run = async function (request) {

    const server = request.server;
    const knex = server.app.knex;

        return knex
            .then((knexInstance) => {

                return knexInstance.select().table('crud')
                    .then((data) => {

                        return Promise.resolve(data);
                    })
            })
            .catch((error) => {

                return Promise.resolve(Boom.badRequest(error.detail));
            });
};
