'use strict';

exports.serverConfig = {
    host: 'localhost',
    port: '8080'
};

exports.storeConfig = {
    client: 'pg',
    options: {
        host : '127.0.0.1',
        user : 'postgres',
        password : 'postgres',
        database : 'postgres'
    }
}
