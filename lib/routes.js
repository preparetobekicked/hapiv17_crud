'use strict';

const Register = require('./handlers/register');
const View = require('./handlers/view');
const Update = require('./handlers/update');
const Delete = require('./handlers/delete');

exports.load = [
    { path: '/register', method: 'POST', handler: Register.run },
    { path: '/view', method: 'GET', handler: View.run },
    { path: '/update', method: 'PUT', handler: Update.run },
    { path: '/delete', method: 'DELETE', handler: Delete.run },
];
