'use strict';


exports.setupStore = async function (options) {

    const knex = require('knex')({
        client: 'pg',
        connection: options
    });

    const createTable = await knex.schema.hasTable('crud')
        .then((exists) => {

            if (!exists) {
                knex.schema.withSchema('public').createTable('crud', (userTable) => {

                    userTable.increments();
                    userTable.string('name').notNullable();
                    userTable.string('email', 128).notNullable().unique();
                    userTable.string('password').notNullable();
                    userTable.timestamp('created_at').defaultTo(knex.fn.now());
                    userTable.timestamp('updated_at').defaultTo(knex.fn.now());
                })
                    .then(() => {

                        return Promise.resolve(knex);
                    })
                    .catch((error) => {

                        return Promise.reject(error);
                    })
            }
            else {
                return Promise.resolve(knex);
            }
        });

    return createTable;
};
