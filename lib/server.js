'use strict';

const Hapi = require('hapi');
const Config = require('./config');
const Store = require('./store');
const Routes = require('./routes');

const server = new Hapi.Server(Config.serverConfig);

const store = {
    knex: Store.setupStore(Config.storeConfig.options)
};

server.app = Object.assign(server.app, Config.app, store);

server.bind({ server: server });

const init = Promise.all([
    server.route(Routes.load),
    server
]);

module.exports = init;
